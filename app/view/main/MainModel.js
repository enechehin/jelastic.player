/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('JPlayer.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        q: '',
        players: {}
    }

});
