/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 */
Ext.define('JPlayer.view.main.Main', {
    extend: 'Ext.dataview.List',
    xtype: 'app-main',

    requires: [
        'JPlayer.view.main.MainController',
        'JPlayer.view.main.MainModel'
    ],

    controller: 'main',
    viewModel: 'main',

    title: 'jPlayer',
    
    itemTpl: '<div class="item x-item-no-select {status}">'+
                '<div class="item-cover" style="background-image: url({artwork_url})"></div>' + 
                '<div class="item-title">{title}</div>' + 
                '<div class="item-controls">' + 
                    '<button class="btn-play x-fa fa-play"></button>' + 
                    '<button class="btn-stop x-fa fa-stop"></button>' +
                '</div>' + 
             '</div>',
     
    listeners: {
        childtap: 'listItemClick'
    }, 

    data: [],
    
    items: [
        {
            xtype: 'toolbar',
            docked: 'top',
            layout: 'hbox',
            ui: 'searchpanel',
            items: [
                {
                    xtype: 'textfield',
                    name: 'q',
                    flex: 1,
                    bind: '{q}',
                    border: true,
                    placeholder: 'Enter song title or author name...',
                    listeners: {
                        keyup: 'keyupSearchField'
                    }
                },
                {
                    xtype: 'button',
                    text: 'Search',
                    listeners: {
                        tap: 'search'
                    }
                }
            ]
        }
    ],
    
    emptyText: 'Nothing found...'


});
