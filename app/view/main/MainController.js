/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 */
Ext.define('JPlayer.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',
        
    init: function() {

        SC.initialize({
            client_id: 'bf8071049de82de2163853fd725bf64c'
        });
        
        this.search();
    },

    search: function() {

        var _this = this;
        var q = this.getViewModel().data.q;
        
        SC.get('/tracks', { q: q }).then(function(tracks){
            _this.getView().setData(tracks);
            _this.getView().unmask();
        });

    },
    
    keyupSearchField: function(field, event){
        if (event.keyCode === 13) {
            this.search();
        }
    },
    
    listItemClick: function(t, location){

        var clicked = Ext.get(location.sourceElement);
        var itemId = location.item.getRecord().id;
    
        if (clicked.hasCls('btn-play')) {            
            this.play(itemId);
        }
        
        if (clicked.hasCls('btn-stop')) {
            this.stop(itemId);
        }
        
        return false;
    },
    
    /**
     * Start playing song
     * 
     * @param {Number} id
     * @returns {void}
     */
    play: function(id) {
        
        var _this = this;
        
        SC.stream('/tracks/' + id).then(function(player){
            player.play();
            _this.getViewModel().data.players[id] = player;
        });
    },
    
    /**
     * Stop playing song
     * 
     * @param {Number} id
     * @returns {void}
     */
    stop: function(id) {
        
        var players = this.getViewModel().data.players;
        
        if (typeof players[id] !== 'undefined') {
            players[id].pause();
        }
    }
    
});
